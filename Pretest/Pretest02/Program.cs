﻿namespace Pretest02
{
    public class Program
    {
        public Program()
        {
            Menu();
        }
        static void Main(string[] args)
        {
            Menu();
        }
        private static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("=== Pretest 01 ===");
            Console.WriteLine("| 1. Soal 04     |");
            Console.WriteLine("| 2. Soal 05     |");
            Console.WriteLine("| 3. Soal 06     |");


            Console.WriteLine();
            Console.Write("Masukkan no soal : ");
            int soal = int.Parse(Console.ReadLine());

            string answer = "t";
            while (answer.ToLower() == "t")
            {
                switch (soal)
                {
                    case 1:
                        soal04 soal4 = new soal04();
                        kembali();
                        break;
                    case 2:
                        soal05 soal2 = new soal05();
                        kembali();
                        break;
                    case 3:
                        soal06 soal3 = new soal06();
                        kembali();
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
                Console.Write("Press any key...");
                Console.WriteLine();
                Console.ReadKey();
            }
            static void kembali()
            {
                Console.Write("\nApakah ingin kembali ke menu ? (y/n) ");
                string menuInput = Console.ReadLine().ToLower();
                if (menuInput == "y")
                {
                    Console.Clear();
                    Menu();
                }
                else
                {
                    Console.WriteLine("Terima kasih!");
                    Console.WriteLine("Silahkan tekan Enter kembali...");
                }
            }
        }
    }
}
