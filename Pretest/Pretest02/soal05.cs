﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
    internal class soal05
    {
        public soal05()
        {
            Console.WriteLine();
            Console.Write("Masukkan n : ");
            int n = int.Parse(Console.ReadLine());

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    if (i == n / 2 && j == n / 2)
                        Console.Write($"{n}\t");
                    else if (i == 0 || i == n - 1 || j == 0 || j == n - 1 || i == j || j == n - i - 1)
                        Console.Write("*\t");
                    else
                        Console.Write("\t");

                }
            }
        }
    }
}
