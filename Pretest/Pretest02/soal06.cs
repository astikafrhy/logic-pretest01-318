﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
    internal class soal06
    {
        public soal06()
        {
            //int bayar = 0;
            Console.WriteLine();
            Console.WriteLine("=== Parkir ===");
            Console.Write("Tanggal Masuk  : ");
            string[] dtAwal = Console.ReadLine().Split('T');
            string[] tglAwal = dtAwal[0].Split('-');
            string[] wktAwal = dtAwal[1].Split(':');
            DateTime dateAwal = new DateTime(int.Parse(tglAwal[0]), int.Parse(tglAwal[1]), int.Parse(tglAwal[2]),
                                int.Parse(wktAwal[0]), int.Parse(wktAwal[1]), int.Parse(wktAwal[2]));

            Console.Write("Tanggal Keluar : ");
            string[] dtAkhir = Console.ReadLine().Split('T');
            string[] tglAkhir = dtAkhir[0].Split('-');
            string[] wktAkhir = dtAkhir[1].Split(':');
            DateTime dateAkhir = new DateTime(int.Parse(tglAkhir[0]), int.Parse(tglAkhir[1]), int.Parse(tglAkhir[2]),
                                 int.Parse(wktAkhir[0]), int.Parse(wktAkhir[1]), int.Parse(wktAkhir[2]));

            TimeSpan dt = dateAkhir - dateAwal;
            int jam = Convert.ToInt16(Math.Ceiling(dt.TotalHours));
            int hari = jam / 24;
            jam = jam - (hari * 24);
            Console.WriteLine($"hari : {hari}, jam :{jam}");

            double tarif = hari * 20000;
            for(int i = 0; i <= jam ; i++)
            {
                if (i == 1)
                    tarif += 5000;
                else if (i >= 2 && i <= 7)
                    tarif += 3000;
                else if (i >= 8 && i <= 12)
                    tarif += 2000;
                else if (i >= 13 && i <= 23)
                    tarif += 1000;
            }
            Console.WriteLine($"tarif : {tarif}");
            Console.ReadKey();
        }
    }
}
