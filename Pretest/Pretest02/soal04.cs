﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
    internal class soal04
    {

        public soal04()
        {
            Console.Write("\nPinjam buku (dd/mm/yyyy) : ");
            string tanggalPertama = Console.ReadLine();
            Console.Write("\nKembali (dd/mm/yyyy) : ");
            string tanggalMengembalikan = Console.ReadLine();
            DateTime tanggalPertamaMinjam = DateTime.Parse(tanggalPertama);
            DateTime mengembalikanTanggal = DateTime.Parse(tanggalMengembalikan);

            TimeSpan span = mengembalikanTanggal - tanggalPertamaMinjam;

            int mtk = 750, struktur = 1500, kalkulus = 1000;
            int denda = 0;

            if (span.Days <= 0)
            {
                Console.WriteLine("Masukkan tanggal awal pinjam dengan benar!");
            }
            else if (span.Days == 4)
            {
                denda = mtk * (span.Days - 4);
            }
            else if (span.Days > 5 && span.Days <= 7)
            {
                denda = (mtk * (span.Days - 4)) + (kalkulus * (span.Days - 5));
            }
            else if (span.Days > 7)
            {
                denda = (mtk * (span.Days - 4)) + (kalkulus * (span.Days - 5)) + (struktur * (span.Days - 7));
            }
            Console.WriteLine($"Total hari : {span.Days}");
            Console.WriteLine($"Denda : {denda}");

        }
    }

}
       

