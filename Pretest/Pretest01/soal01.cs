﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class soal01
    {
        public soal01()
        {
            Console.WriteLine();
            Console.WriteLine("=== SOAL 01 ===");
            Console.Write("Masukkan Topi   : ");
            int[] topi = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
           
            Console.Write("Masukkan Kemeja : ");
            int[] kemeja = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

            int maks = topi[0] + kemeja[0], min = maks;

            for (int i = 0; i < topi.Length; i++)
            {
                for (int j = 0; j < kemeja.Length; j++)
                {
                    int total = topi[i] + kemeja[j];
                    if (maks < total)
                        maks = total;
                    if (min > total)
                        min = total;
                    //Console.WriteLine();
                   // Console.WriteLine(total);
                }
            }
            Console.WriteLine();
            Console.WriteLine($"jumlah tertinggi = {maks}");
            Console.WriteLine($"jumlah terendah = {min}");
            
        }
    }
}
