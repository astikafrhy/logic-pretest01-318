﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class soal03
    {
        public soal03()
        { 
            // inputan : s9nn10
            // output  : nns019
            Console.WriteLine();
            Console.WriteLine("=== SOAL 03 ===");
            Console.Write("Inputan : ");
            char[] kata = Console.ReadLine().ToCharArray();

            string pustaka = "abcdefghijklmnopqrstuvwxyz0123456789";

            for(int i = 1; i < kata.Length; i++)
            {
                for( int j = i; j > 0; j--)
                {
                    if (pustaka.IndexOf(kata[j]) < pustaka.IndexOf(kata[j - 1]))
                    {
                        var temp = kata[j];
                        kata[j] = kata[j - 1];
                        kata[j - 1] = temp;
                    }
                }
            }
            foreach(var item in kata)
            {
                Console.Write(item);
            }
        }
    }
}
