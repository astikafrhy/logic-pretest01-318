﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class soal02
    {
        public soal02()
        {
            Console.WriteLine();
            Console.WriteLine("=== SOAL 02 ===");
            Console.Write("Input kata yang dicari : ");
            string cari = Console.ReadLine().ToLower();
            Console.Write("Input kalimat          : ");
            string kalimat = Console.ReadLine().ToLower();

            int index = 0;

            for (int i = 0; i < kalimat.Length; i++)
            {
                if (index < cari.Length && kalimat[i] == cari[index])
                {
                    index++;
                }
            }
            Console.WriteLine(index == cari.Length ? "YES" : "NO");

        }
    }
}
