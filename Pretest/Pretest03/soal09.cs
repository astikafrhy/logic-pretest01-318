﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest03
{
    internal class soal09
    {
        public soal09()
        {
            Console.Write("Masukan inputan: ");
            int[] input = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int naik = 0, turun = 0;
            for (int i = 1; i < input.Length - 1; i++)
            {
                if (input[i] > input[i - 1] && input[i] > input[i + 1])
                {
                    naik++;
                }
                else if (input[i] < input[i - 1] && input[i] < input[i + 1])
                    turun++;
            }

            Console.WriteLine($"naik: {naik}");
            Console.WriteLine($"turun: {turun}");

        }
    }
}
