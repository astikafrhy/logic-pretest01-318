﻿namespace Pretest03
{
    public class Program
    {
        public Program()
        {
            Menu();
        }
        static void Main(string[] args)
        {
            Menu();
        }
        private static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("=== Pretest 01 ===");
            Console.WriteLine("| 1. Soal 07     |");
            Console.WriteLine("| 2. Soal 08     |");
            Console.WriteLine("| 3. Soal 09     |");


            Console.WriteLine();
            Console.Write("Masukkan no soal : ");
            int soal = int.Parse(Console.ReadLine());

            string answer = "t";
            while (answer.ToLower() == "t")
            {
                switch (soal)
                {
                    case 1:
                        soal07 soal4 = new soal07();
                        kembali();
                        break;
                    case 2:
                        soal08 soal2 = new soal08();
                        kembali();
                        break;
                    case 3:
                        soal09 soal3 = new soal09();
                        kembali();
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
                Console.Write("Press any key...");
                Console.WriteLine();
                Console.ReadKey();
            }
            static void kembali()
            {
                Console.Write("\nApakah ingin kembali ke menu ? (y/n) ");
                string menuInput = Console.ReadLine().ToLower();
                if (menuInput == "y")
                {
                    Console.Clear();
                    Menu();
                }
                else
                {
                    Console.WriteLine("Terima kasih!");
                    Console.WriteLine("Silahkan tekan Enter kembali...");
                }
            }
        }
    }
}
