﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class soal04
    {
        public soal04()
        {
            Console.WriteLine("=== Soal04 ===");
			Console.Write("Inputkan kata : ");
			char[] input = Console.ReadLine().ToCharArray().Distinct().ToArray();


			string pustaka = "abcdefghijklmnopqrstuvwxyz0123456789";

			for (int i = 1; i < input.Length; i++)
			{
				for (int j = i; j > 0; j--)
				{
					if (pustaka.IndexOf(input[j]) < pustaka.IndexOf(input[j - 1]))
					{
						var tamp = input[j];
						input[j] = input[j - 1];
						input[j - 1] = tamp;
					}
				}
			}

			foreach (var item in input)
			{
				Console.Write(item);
			}
		}
    }
}
