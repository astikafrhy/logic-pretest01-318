﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class soal08
    {
        public soal08()
        {
            Console.Write("\tMasukan jumlah bilangan angka: ");
            int range = int.Parse(Console.ReadLine());

            int[] angkaFib = new int[range];
            for (int i = 0; i < range; i++)
            {
                Console.Write("\t");
                if (i <= 1)
                {
                    angkaFib[i] = 1;
                }
                else
                {
                    angkaFib[i] = angkaFib[i - 2] + angkaFib[i - 1];
                }
                
            }
            Console.Write(angkaFib[range - 1]);
            Console.Write(string.Join(" ", angkaFib));

        }
    }
}
