﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class soal02
    {
        public soal02()
        {
            Console.Write("Inputan (pakai spasi): ");
            int[] input = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int genap = 0;
            int ganjil = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] % 2 == 0)
                {
                    genap += input[i];
                }
                else if (input[i] % 2 == 1)
                {
                    ganjil += input[i];
                }
            }
            int hasil = genap - ganjil;
            Console.WriteLine($"Hasil : Hasil Genap {genap} - Hasil Ganjil {ganjil} = {hasil}");
        }
    }
}
