﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class soal05
    {
        public soal05()
        {

            int maks = 0;
            Console.WriteLine("== Soal 05 ==");
            Console.Write("Uang    : ");
            int uang = int.Parse(Console.ReadLine());

            Console.Write("Harga 1 : ");
            int[] harga1 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            Console.Write("Harga 2 : ");
            int[] harga2 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            Console.Write("harga 3 : ");
            int[] harga3 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

            for (int i = 0; i < harga1.Length; i++)
            {
                for (int j = 0; j < harga2.Length; j++)
                {
                    for (int k = 0; k < harga3.Length; k++)
                    {
                        int totalHarga = harga1[i] + harga2[j] + harga3[k];
                        if (totalHarga <= uang && totalHarga >= maks)
                        {
                            maks = totalHarga;
                        }
                           
                    }
                }
            }

            Console.WriteLine($"{maks}");

        }
        
    }

}
