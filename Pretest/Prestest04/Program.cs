﻿namespace Pretest04
{
    public class Program
    {
        public Program()
        {
            Menu();
        }
        static void Main(string[] args)
        {
            Menu();
        }
        private static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("=== Pretest 04 ===");
            Console.WriteLine("| 1. Soal 01    |");
            Console.WriteLine("| 2. Soal 02    |");
            Console.WriteLine("| 3. Soal 03    |");
            Console.WriteLine("| 4. Soal 04    |");
            Console.WriteLine("| 5. Soal 05    |");
            Console.WriteLine("| 6. Soal 06    |");
            Console.WriteLine("| 7. Soal 07    |");
            Console.WriteLine("| 8. Soal 08    |");


            Console.WriteLine();
            Console.Write("Masukkan no soal : ");
            int soal = int.Parse(Console.ReadLine());

            string answer = "t";
            while (answer.ToLower() == "t")
            {
                switch (soal)
                {
                    case 1:
                        soal01 soal1 = new soal01();
                        kembali();
                        break;
                    case 2:
                        soal02 soal2 = new soal02();
                        kembali();
                        break;
                    case 3:
                        soal03 soal3 = new soal03();
                        kembali();
                        break;
                    case 4:
                        soal04 soal4 = new soal04();
                        kembali();
                        break;
                    case 5:
                        soal05 soal5 = new soal05();
                        kembali();
                        break;
                    case 6:
                        soal06 soal6 = new soal06();
                        kembali();
                        break;
                    case 7:
                        soal07 soal7 = new soal07();
                        kembali();
                        break;
                    case 8:
                        soal08 soal8 = new soal08();
                        kembali();
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
                Console.Write("Press any key...");
                Console.WriteLine();
                Console.ReadKey();
            }
            static void kembali()
            {
                Console.Write("\nApakah ingin kembali ke menu ? (y/n) ");
                string menuInput = Console.ReadLine().ToLower();
                if (menuInput == "y")
                {
                    Console.Clear();
                    Menu();
                }
                else
                {
                    Console.WriteLine("Terima kasih!");
                    Console.WriteLine("Silahkan tekan Enter kembali...");
                }
            }
        }
    }
}
